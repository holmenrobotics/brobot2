package org.usfirst.frc.team6166.robot;

public class AutoStraight {
	private AutoMethods util;
	private int step = 0;
	private boolean vStep = false;
	private double driveTill = 0.0;

	public AutoStraight(AutoMethods autoMethod) {
		util = autoMethod;
	}

	public boolean execute() {
		switch (step) {
		case 0:
			util.initializeAuto();
			step++;
			driveTill = util.getTime() + 7;
			break;
		case 1: // Will drive straight forward only
			if (util.driveStraightForward(driveTill, 0.3)) {
				step++;
			}
			break;
		default:
			vStep = false;
			break;
		}
		return vStep;
	}
}

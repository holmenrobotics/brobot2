package org.usfirst.frc.team6166.robot;

import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.Counter;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.PowerDistributionPanel;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.Spark;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.VictorSP;

public class AutoMethods {
	private RobotDrive bot;
	private Timer timer;
	private ADXRS450_Gyro gyro;
	private VictorSP arm;
	private Counter switchUp;
	private Counter switchDown;
	private DoubleSolenoid solenoid;
	// private UltrasonicSensor ultrasound;
	private Spark intake;
	private PowerDistributionPanel pdp;
	private Boolean runThread = true;
	private Joystick stick;
	private VictorSP climber;
	private double armTime = System.currentTimeMillis() + 200000;
	String armPos = "";

	/**
	 * 
	 * @param robot
	 *            RobotDrive Object used to control main robot movements
	 * @param arm
	 *            VictorSP object used to control up and down movement of arm
	 * @param Up
	 *            Digital Input object for the switch detecting if the arm is in
	 *            the up position
	 * @param Down
	 *            Digital Input object for the switch detecting if the arm is in
	 *            the down position
	 * @param armRollers
	 *            Motor variable used to run the intake wheels on the arm
	 * @param ultraSonicSensor
	 *            Variable containing the custom object made to control read the
	 *            ultrasonic sensor
	 * @param stick
	 *            Joystick variable for the controller used ton manipulate the
	 *            robot during teleop
	 * @param climber
	 *            Climber motor variable used to run the climber at the end of
	 *            the match, this var is used to override the climber when it
	 *            stalls.
	 */
	public AutoMethods(RobotDrive robot, VictorSP vArm, DigitalInput Up, DigitalInput Down, Spark armRollers,
			Joystick stick, VictorSP climber) {
		bot = robot;
		this.solenoid = solenoid;
		timer = new Timer();
		gyro = new ADXRS450_Gyro();
		switchUp = new Counter(Up);
		switchDown = new Counter(Down);
		pdp = new PowerDistributionPanel();
		// ultrasound = ultraSonicSensor;
		intake = armRollers;
		gyro.calibrate();
		gyro.reset();
		arm = vArm;
		switchUp.reset();
		switchDown.reset();
		this.stick = stick;

		checkThread();
	}

	/**
	 * 
	 * @param robot
	 *            RobotDrive Object used to control main robot movements
	 * @param vArm
	 *            VictorSP object used to control up and down movement of arm
	 * @param Up
	 *            Digital Input object for the switch detecting if the arm is in
	 *            the up position
	 * @param Down
	 *            Digital Input object for the switch detecting if the arm is in
	 *            the down position
	 * @param solenoid
	 *            Double Solenoid Object used to control compressing and
	 *            expanding the arm
	 * @param ultraSonicSensor
	 *            Variable containing the custom object made to control read the
	 *            ultrasonic sensor
	 * @param stick
	 *            Joystick variable for the controller used ton manipulate the
	 *            robot during teleop
	 * @param climber
	 *            Climber motor variable used to run the climber at the end of
	 *            the match, this var is used to override the climber when it
	 *            stalls.
	 */
	public AutoMethods(RobotDrive robot, VictorSP vArm, DigitalInput Up, DigitalInput Down, DoubleSolenoid solenoid,
			Joystick stick, VictorSP climber) {
		bot = robot;
		this.solenoid = solenoid;
		timer = new Timer();
		gyro = new ADXRS450_Gyro();
		switchUp = new Counter(Up);
		switchDown = new Counter(Down);
		pdp = new PowerDistributionPanel();
		// ultrasound = ultraSonicSensor;
		gyro.calibrate();
		gyro.reset();
		arm = vArm;
		switchUp.reset();
		switchDown.reset();
		this.stick = stick;
		checkThread();
	}

	private void checkThread() {
		Thread t = new Thread(() -> {
			while (runThread) {
				if (System.currentTimeMillis() > armTime) {
					armTime = System.currentTimeMillis() + 200000;
					stopArm();
					armPos = "down";
				} else if (isSwitchUp() && !armPos.equals("up")) {
					stopArm();
					armPos = "up";
				}

				/*
				 * if (pdp.getCurrent(5) > 25 && !stick.getRawButton(2)) {
				 * systemTime = System.currentTimeMillis() + 1000;
				 * stick.setRumble(RumbleType.kLeftRumble, 1); climber.set(0.0);
				 * }
				 */

			}
		});
		t.start();
	}

	/**
	 * @Does Starts and activates all methods needed for controlling various
	 *       autonomous functions
	 */
	public void initializeAuto() {
		timer.start();
		gyroReset();
	}

	/**
	 * Resets Gyro to 0.0 degrees
	 */
	public void gyroReset() {
		gyro.reset();
	}

	/**
	 * @Does Stops autonomous Timer
	 */
	public void stopAuto() {
		timer.stop();
	}

	/**
	 * Sets Solenoid so that it will close the arm
	 */
	public void closeArm() {
		solenoid.set(DoubleSolenoid.Value.kReverse);
	}

	/**
	 * Sets Solenoid so that the arm will open
	 */
	public void openArm() {
		solenoid.set(DoubleSolenoid.Value.kForward);
	}

	/**
	 * 
	 * @return Current Time elapsed from start of autonomous
	 */
	public double getTime() {
		return timer.get();
	}

	/**
	 * 
	 * @return Current angle of gyroscope with limits being -360 >= angle >= 360
	 */
	public double getAngle() {
		return (gyro.getAngle() % 360);
	}

	// sets the total distance to a variable for future use
	/**
	 * 
	 * @param toAngle
	 *            the angle that the robot will turn to with a 10 degree buffer
	 *            both directions
	 * @return Will return true once robot is within the bounds of the requested
	 *         angle
	 */
	public boolean turn(double toAngle) {
		double angle = getAngle();
		if (angle < (toAngle - 5)) {
			bot.drive(0.3, -1.0);
			return false;
		} else if (angle > (toAngle + 5)) {
			bot.drive(0.3, 1.0);
			return false;
		} else {
			return true;
		}
	}

	/**
	 * 
	 * @param runTo
	 *            Robot will drive until the timer returns this value or greater
	 * @param speed
	 *            Speed that robot will drive at on scale of 0.0 to 1
	 * @return Will return true once robot has reached the time it was set to
	 *         run until
	 */
	public boolean driveStraightForward(double runTo, double speed) {
		if (timer.get() < runTo) {
			double angle = getAngle();
			System.out.println("Angle = " + angle);
			bot.drive(-speed, -(angle) * .00003);
			return false;
		} else {
			return true;
		}
	}

	/**
	 * 
	 * @param runTo
	 *            Robot will drive until the timer returns this value or greater
	 * @param speed
	 *            Speed that robot will drive at on scale of 0.0 to 1
	 * @return Will return true once robot has reached the time it was set to
	 *         run until
	 */
	public boolean driveStraightBackward(double runTo, double speed) {
		if (timer.get() < runTo) {
			double angle = getAngle();
			System.out.println("Angle = " + angle);
			bot.drive(speed, (angle) * -.000003);
			return false;
		} else {
			return true;
		}
	}

	/**
	 * 
	 * @param distance
	 * @param speed
	 * @return
	 */
	public boolean driveStraightDist(double distance, double speed) {
		if (getDist() > distance) {
			double angle = getAngle();
			System.out.println("Angle = " + angle);
			bot.drive(-speed, -(angle) * .000003);
			return false;
		} else {
			return true;
		}
	}

	public boolean driveAndSlow(double distance, double speed, double originalDist) {
		if (getDist() > distance) {
			double angle = getAngle();
			bot.drive(-speed * ((getDist() - distance) / (originalDist) - distance), -(angle) * .000003);
			return false;
		} else {
			return true;
		}
	}

	public void stopArm() {
		arm.set(0.0);
	}

	/**
	 * 
	 * @return Will return true once the arm has reached the Up position
	 */
	public boolean armDown() {
		armTime = System.currentTimeMillis() + 2000;
		arm.set(0.2);
		return true;
	}

	public boolean waitTime(double waitTill) {
		if (getTime() < waitTill) {
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @return Will return true once the arm has reached the down position
	 */
	public boolean armUp() {
		if (!isSwitchUp()) {
			arm.set(-0.4);
			return false;
		} else {
			stopArm();
			return true;
		}
	}

	/**
	 * 
	 * @return Returns whether the switch at the up position has been triggered
	 */
	public boolean isSwitchUp() {
		if (switchUp.get() > 0) {
			switchUp.reset();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 
	 * @param waitTime
	 *            input currentTime of first call of this method plus 0.2
	 * @return Returns True when arm is closed and in the up position
	 */
	public boolean autoArmInitilize(double waitTime) {
		/*
		 * if (getTime() > waitTime) { return armUp(); } else { closeArm();
		 * return false; }
		 */
		return true;
	}

	/**
	 * 
	 * @return Returns weather the switch at the down position has been
	 *         triggered
	 */
	public boolean isSwitchDown() {
		if (switchDown.get() > 0) {
			switchDown.reset();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @does Intake Wheels on arm will pull gear in
	 */
	public void intakeIn() {
		intake.set(-0.7);
	}

	/**
	 * @does Intake Wheels on arm will push gear out
	 */
	public void intakeOut() {
		intake.set(0.6);
	}

	/**
	 * @does Intake Wheels will be disabled and turned off
	 */
	public void disableIntake() {
		intake.setSpeed(0.0);
	}

	/**
	 * 
	 * @return distance in inches
	 */
	public double getDist() {
		// return ultrasound.getDistIn();
		return 0.0;
	}
}
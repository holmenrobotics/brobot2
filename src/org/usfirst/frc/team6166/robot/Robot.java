package org.usfirst.frc.team6166.robot;

import edu.wpi.first.wpilibj.CameraServer;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.PowerDistributionPanel;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.Spark;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
public class Robot extends IterativeRobot {
	// Objects for Robot Components
	RobotDrive myRobot = new RobotDrive(1, 0, 2, 3);
	VictorSP arm = new VictorSP(5);
	VictorSP climber = new VictorSP(4);
	Spark armRollers = new Spark(6);
	DigitalInput switchUp = new DigitalInput(0);
	DigitalInput switchDown = new DigitalInput(1);
	// Compressor compressor = new Compressor(0);
	// DoubleSolenoid solenoid = new DoubleSolenoid(5, 4);
	// UltrasonicSensor ultrasound = new UltrasonicSensor(2);
	PowerDistributionPanel pdp = new PowerDistributionPanel();

	// Objects/Vars for Computer to Robot Interactions
	Joystick stick = new Joystick(0);
	Joystick altStick = new Joystick(1);
	static SendableChooser<Integer> chooser = new SendableChooser<>();
	// Objects/Vars for Automatic controls
	AutoMethods util;
	int autoSelection;
	AutoRight autoRight;
	AutoCenter autoCenter;
	AutoLeft autoLeft;
	AutoStraight autoStraight;
	AutoRecordRunner autoRecorder = new AutoRecordRunner(stick);
	AutoStickRep autoStick = new AutoStickRep();
	boolean checkAuto;
	// Vars for internal Checks and Controls
	private boolean runArmUp = true;
	private boolean runArmDown = true;

	/**
	 * This function is run when the robot is first started up and should be
	 * used for any initialization code.
	 */
	@Override
	public void robotInit() {
		// Currently all of these need to be inverted for it to work
		myRobot.setInvertedMotor(RobotDrive.MotorType.kFrontLeft, true);
		myRobot.setInvertedMotor(RobotDrive.MotorType.kRearLeft, true);
		myRobot.setInvertedMotor(RobotDrive.MotorType.kFrontRight, true);
		myRobot.setInvertedMotor(RobotDrive.MotorType.kRearRight, true);
		myRobot.setExpiration(0.1);
		// compressor.setClosedLoopControl(true);
		CameraServer.getInstance().startAutomaticCapture().setResolution(320, 240);
		// util = new AutoMethods(myRobot, arm, switchUp, switchDown, solenoid,
		// ultrasound, stick, climber);
		util = new AutoMethods(myRobot, arm, switchUp, switchDown, armRollers, stick, climber);
		createSelector();

	}

	/**
	 * Creates selector and options for autonomous, then sends them to the user.
	 */
	public void createSelector() {
		chooser.addDefault("Nothing", 100);
		chooser.addObject("Left", 1);
		chooser.addObject("Center", 2);
		chooser.addObject("Right", 3);
		chooser.addObject("Forward", 4);
		SmartDashboard.putData("Auto Selector", chooser);
	}

	/**
	 * 
	 * @return integer value for autonomous mode selected in the Dashboard
	 */
	public static int getSelection() {
		return (int) chooser.getSelected();
	}

	/**
	 * Runs once at the beginning of autonomous and sets up the correct
	 * autonomous mode
	 */
	public void autonomousInit() {
		// autoRecorder.runThread(autoStick);
		autoSelection = getSelection();
		switch (autoSelection) {
		case 1:
			autoLeft = new AutoLeft(util);
			break;
		case 2:
			autoCenter = new AutoCenter(util);
			break;
		case 3:
			autoRight = new AutoRight(util);
			break;
		case 4:
			autoStraight = new AutoStraight(util);
		default:
			break;
		}
	}

	/**
	 * This function is called periodically during autonomous
	 */
	@Override
	public void autonomousPeriodic() {
		System.out.println("Running Auto Periodic");
		switch (autoSelection) {
		case 1:
			checkAuto = autoLeft.execute();
			break;
		case 2:
			checkAuto = autoCenter.execute();
			break;
		case 3:
			checkAuto = autoRight.execute();
			break;
		case 4:
			checkAuto = autoStraight.execute();
			break;
		default:
			myRobot.drive(0.0, 0.0);
			break;
		}
		if (checkAuto) {
			autoSelection = 10;
		}
	}

	/*
	 * public void autonomousPerioid() { if (autoStick.getRawAxis(1) > 0.06 ||
	 * autoStick.getRawAxis(0) > 0.06) {
	 * myRobot.arcadeDrive(autoStick.getRawAxis(1), autoStick.getRawAxis(0)); }
	 * else { myRobot.arcadeDrive(autoStick.getRawAxis(5) * 0.75,
	 * autoStick.getRawAxis(4) * 0.75); }
	 * 
	 * if (autoStick.getRawButton(4) && ((pdp.getCurrent(5) < 25) ||
	 * autoStick.getRawButton(2))) {// Fix // amps
	 * climber.set(autoStick.getRawAxis(3)); } else if ((autoStick.getRawAxis(2)
	 * > .5) || !runArmUp) { // Button LT on xbox, 2 in code climber.set(0.0);
	 * runArmUp = util.armUp(); } else if ((autoStick.getRawAxis(3) > .5) ||
	 * !runArmDown) { // Button RT on xbox, 3 in code climber.set(0.0);
	 * runArmDown = util.armDown(); } else { util.stopArm(); climber.set(0.0); }
	 * 
	 * if (stick.getRawButton(1)) { // button A util.disableIntake();
	 * util.stopArm(); runArmDown = true; runArmUp = true; }
	 * 
	 * if (stick.getRawButton(6)) { // xbox RB util.intakeIn(); //
	 * util.openArm(); } else if (stick.getRawButton(5)) { // xbox LB
	 * util.intakeOut(); // util.closeArm(); } }
	 */
	public void teleopPeriodic() {
		/*
		 * if (altStick.getRawButton(1)) { autoRecorder.setRecord(true);
		 * autoRecorder.recordThread(); } else if (altStick.getRawButton(2)) {
		 * autoRecorder.setRecord(false); }
		 */
		/*
		 * if (stick.getRawAxis(1) > 0.06 || stick.getRawAxis(0) > 0.06) {
		 * myRobot.arcadeDrive(stick.getRawAxis(1), stick.getRawAxis(0)); } else
		 * { myRobot.arcadeDrive(stick.getRawAxis(5) * 0.75, stick.getRawAxis(4)
		 * * 0.75); }
		 */
		// double xb = stick.getRawAxis(0);
		// double yb = stick.getRawAxis(1);
		// double xs = stick.getRawAxis(4);
		// double ys = stick.getRawAxis(5);
		/*
		 * if (yb > ys || (xb > xs)) { myRobot.arcadeDrive(yb, xb); } else {
		 * myRobot.arcadeDrive(ys * 0.75, stick.getRawAxis(4) * 0.75); }
		 */
		myRobot.arcadeDrive(stick.getRawAxis(5), stick.getRawAxis(4));
		if (stick.getRawButton(4)) {// Fix ((pdp.getCurrent(5) < 25) ||
									// stick.getRawButton(2))
									// amps
			climber.set(-stick.getRawAxis(3));
		} else if ((stick.getRawAxis(2) > .5) || !runArmUp) {
			// Button LT on xbox, 2 in code
			climber.set(0.0);
			runArmUp = util.armUp();
		} else if ((stick.getRawAxis(3) > .5) || !runArmDown) {
			// Button RT on xbox, 3 in code
			climber.set(0.0);
			runArmDown = util.armDown();
		} else {
			util.stopArm();
			climber.set(0.0);
		}

		if (stick.getRawButton(3)) {
			armRollers.set(0.6);
			util.armDown();
		}

		if (stick.getRawButton(1)) {
			// button A
			util.disableIntake();
			util.stopArm();
			armRollers.set(0.0);
			climber.set(0.0);
			runArmDown = true;
			runArmUp = true;
		}

		if (stick.getRawButton(5)) {
			// xbox RB
			armRollers.set(-0.7);
			// util.intakeIn();
			// util.openArm();
		} else if (stick.getRawButton(6)) {
			// xbox LB
			// util.intakeOut();
			armRollers.set(0.6);
			// util.closeArm();
		} else {
			armRollers.set(0.0);
		}
	}
}

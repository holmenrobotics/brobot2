package org.usfirst.frc.team6166.robot;

public class AutoRight {
	private AutoMethods util;
	private int step = 0;
	private boolean vStep = false;
	private boolean next = false;
	private double driveTill = 0.0;

	public AutoRight(AutoMethods autoMethod) {
		util = autoMethod;
	}

	public boolean execute() {
		switch (step) {
		case 0:
			util.initializeAuto();
			step++;
			driveTill = util.getTime() + 3.4;
			break;
		case 1: // Drive Forward
			if (!next) {
				next = util.driveStraightForward(driveTill, 0.3);
			} else {
				next = false;
				step++;
			}
			break;
		case 2: // Turn to -45 degrees (Turn 45 degrees left)
			if (!next) {
				next = util.turn(-67.5);
			} else {
				next = false;
				step++;
				driveTill = util.getTime() + 4;
				util.gyroReset();
			}
			break;
		case 3: // Drive Forward
			if (!next) {
				next = util.driveStraightForward(driveTill, 0.35);
			} else {
				next = false;
				step++;
			}
			break;
		case 4:
			util.armDown();
			util.intakeOut();
			driveTill = util.getTime() + 1;
			step++;
			break;
		case 5:// Drive Backwards
			if (util.driveStraightBackward(driveTill, 0.3)) {
				step++;
			}
			break;
		case 6: // Stop All
			util.stopAuto();
			step++;
			break;
		default:
			vStep = false;
			break;
		}
		return vStep;
	}

	protected boolean isFinished() {
		return vStep;
	}

}

package org.usfirst.frc.team6166.robot;

public class AutoCenter {
	private AutoMethods util;
	private int step = 0;
	private boolean vStep = false;
	private double driveTill = 0.0;

	public AutoCenter(AutoMethods autoMethod) {
		util = autoMethod;
	}

	public boolean execute() {
		switch (step) {
		case 0:
			util.initializeAuto();
			step++;
			driveTill = util.getTime() + 0.8;
			break;
		case 1: // Drive Forward
			if (util.driveStraightForward(driveTill, 0.3)) {
				step++;
			}
			break;
		case 2:// Arm Down
			util.armDown();
			util.intakeOut();
			driveTill = util.getTime() + .3;
			step++;
			break;
		case 3:// Drive Backwards
			if (util.driveStraightBackward(driveTill, 0.3)) {
				step++;
			}
			break;
		case 4: // Stop All
			util.stopAuto();
			step++;
			break;
		default:
			vStep = false;
			break;
		}
		return vStep;
	}
}

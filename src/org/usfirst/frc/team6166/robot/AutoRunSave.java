package org.usfirst.frc.team6166.robot;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AutoRunSave {
	Map<Integer, ArrayList<Double>> axises = new HashMap<Integer, ArrayList<Double>>();
	Map<Integer, ArrayList<Boolean>> buttons = new HashMap<Integer, ArrayList<Boolean>>();
	String dir;
	File saveFile;

	public AutoRunSave() {

	}

	public void setUpLists() {
		axises.put(0, new ArrayList<Double>());
		axises.put(1, new ArrayList<Double>());
		axises.put(2, new ArrayList<Double>());
		axises.put(3, new ArrayList<Double>());
		axises.put(4, new ArrayList<Double>());
		axises.put(5, new ArrayList<Double>());

		buttons.put(1, new ArrayList<Boolean>());
		buttons.put(2, new ArrayList<Boolean>());
		buttons.put(3, new ArrayList<Boolean>());
		buttons.put(4, new ArrayList<Boolean>());
		buttons.put(5, new ArrayList<Boolean>());
		buttons.put(6, new ArrayList<Boolean>());
	}

	// 0-LSX 1-LSY 2-LT 3-RT 4-RSX 5-RSY
	public void addAxis(double axisOne, double axisTwo, double axisThree, double axisFour, double axisFive,
			double axisSix) {
		axises.get(0).add(axisOne);
		axises.get(1).add(axisTwo);
		axises.get(2).add(axisThree);
		axises.get(3).add(axisFour);
		axises.get(4).add(axisFive);
		axises.get(5).add(axisSix);
	}

	// 1-A 2-B 3-X 4-Y 5-LB 6-RB 7-Back 8-Start 9-LSB 10-RSB (SB Stick Button)
	public void addButton(boolean buttonOne, boolean buttonTwo, boolean buttonThree, boolean buttonFour,
			boolean buttonFive, boolean buttonSix) {
		buttons.get(1).add(buttonOne);
		buttons.get(2).add(buttonTwo);
		buttons.get(3).add(buttonThree);
		buttons.get(4).add(buttonFour);
		buttons.get(5).add(buttonFive);
		buttons.get(5).add(buttonSix);
	}

	public boolean getButton(int button, int index) {
		return buttons.get(button).get(index);
	}

	public double getAxis(int axis, int index) {
		return axises.get(axis).get(index);
	}
}

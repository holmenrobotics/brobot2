package org.usfirst.frc.team6166.robot;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import edu.wpi.first.wpilibj.Joystick;

public class AutoRecordRunner {
	Thread t;
	Joystick controller;
	boolean record = true;
	boolean runAuto = true;
	AutoRunSave recording = new AutoRunSave();
	AutoRunSave recorded;

	public AutoRecordRunner(Joystick controller) {
		this.controller = controller;
	}

	public void setRecord(boolean record) {
		this.record = record;
	}

	public void recordThread() {
		t = new Thread(() -> {
			while (record) {
				recording.addAxis(controller.getRawAxis(0), controller.getRawAxis(1), controller.getRawAxis(2),
						controller.getRawAxis(3), controller.getRawAxis(4), controller.getRawAxis(5));
				recording.addButton(controller.getRawButton(1), controller.getRawButton(2), controller.getRawButton(3),
						controller.getRawButton(4), controller.getRawButton(5), controller.getRawButton(6));
			}
			saveToFile("/home/lvuser/" + Robot.getSelection() + ".txt");
		});
	}

	public void runThread(AutoStickRep autoStick) {
		readIn("/home/lvuser/" + Robot.getSelection() + ".txt");
		t = new Thread(() -> {
			int index = 0;
			while (runAuto) {
				autoStick.setAxis(0, recorded.getAxis(0, index));
				autoStick.setAxis(1, recorded.getAxis(1, index));
				autoStick.setAxis(2, recorded.getAxis(2, index));
				autoStick.setAxis(3, recorded.getAxis(3, index));
				autoStick.setAxis(4, recorded.getAxis(4, index));
				autoStick.setAxis(5, recorded.getAxis(5, index));
				autoStick.setButton(1, recorded.getButton(1, index));
				autoStick.setButton(2, recorded.getButton(2, index));
				autoStick.setButton(3, recorded.getButton(3, index));
				autoStick.setButton(4, recorded.getButton(4, index));
				autoStick.setButton(5, recorded.getButton(5, index));
				autoStick.setButton(6, recorded.getButton(6, index));
				index++;
			}
		});

	}

	public void saveToFile(String destination) {
		File recordFile = new File(destination);
		FileOutputStream fout;
		ObjectOutputStream oos;
		try {
			fout = new FileOutputStream(recordFile);
			oos = new ObjectOutputStream(fout);
			oos.writeObject(recording);
		} catch (Exception e) {
			System.out.print(e);
			System.exit(-9000);
		}
	}

	public void readIn(String location) {
		AutoRunSave obj = new AutoRunSave();
		try {
			FileInputStream fileIn = new FileInputStream(location);
			ObjectInputStream objectIn = new ObjectInputStream(fileIn);
			obj = (AutoRunSave) objectIn.readObject();
		} catch (Exception e) {
			System.out.println(e);
			System.exit(-9000);
		}
		recorded = obj;
	}
}

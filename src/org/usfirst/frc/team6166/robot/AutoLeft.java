package org.usfirst.frc.team6166.robot;

public class AutoLeft {
	private AutoMethods util;
	private int step = 0;
	private double driveTill = 0.0;
	private boolean vStep = false;

	public AutoLeft(AutoMethods autoMethod) {
		util = autoMethod;
	}

	public boolean execute() {
		switch (step) {
		case 0:
			util.initializeAuto();
			driveTill = util.getTime() + 0.2;
			step++;
			break;
		case 1:
			if (util.autoArmInitilize(driveTill)) {
				step++;
				driveTill = util.getTime() + 5.4;
			}
			break;
		case 2: // Drive Forward
			if (util.driveStraightForward(driveTill, 0.3)) {
				step++;
			}
			break;
		case 3: // Turn to -45 degrees (Turn 45 degrees left)
			if (util.turn(57.5)) {
				step++;
				driveTill = util.getTime() + 4;
				util.gyroReset();
			}
			break;
		case 4: // Drive Forward
			if (util.driveStraightForward(driveTill, 0.35)) {
				step++;
			}
			break;
		case 5: // Arm down
			util.armDown();
			util.intakeOut();
			driveTill = util.getTime() + 1;
			util.gyroReset();
			step++;
			break;
		case 6: // Drive back
			if (util.driveStraightBackward(driveTill, 0.3)) {
				step++;
			}
			break;
		case 7: // Turn to 0 degrees (turn right 45 degrees)
			if (util.turn(-40)) {
				step++;
				driveTill = util.getTime() + 1;
			}

			break;
		case 10: // Drive forward
			if (util.driveStraightForward(driveTill, 0.4)) {
				step++;
			}
			break;
		default:
			vStep = false;
			break;
		}
		return vStep;
	}
}
